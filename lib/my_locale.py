msg = {}
__all__ = ( 'msg' )

def module_init():
    import locale
    from os.path import exists
    global msg
    my_lang,my_enc = locale.getlocale(locale.LC_CTYPE)
    lang_file = 'lib/'+my_lang
    if exists(lang_file):
        fp = open(lang_file,'r')
        for ln1 in fp:
            k,v = ln1.split(':',1)
            msg[k] = v
        fp.close()
    elif my_lang == 'zh_CN':
        msg['hit'] = '击中目标'
        msg['miss'] = '没有击中'
        msg['damage'] = '摧毁目标'
        msg['long_damage'] = '目标已经被摧毁'
        msg['ready_fire'] = '可以射击'
        msg['loading'] = '正在装填炮弹'
        msg['count_fire'] = '主炮射击：{0} 次'
        msg['count_hit'] = '命中次数：{0} 次，破坏率：{1:.1f}'
        msg['distance'] = '目标距离:{0}M'
        msg['win'] = '窗口'
        msg['fs'] = '全屏'
        msg['quit'] = '退出'
        msg['fail'] = '任务失败'
        msg['success'] = '你成功于 {0} 米外摧毁敌舰！'
        msg['fire_dist'] = '射击{0}米外的敌人'
    else:
        msg['hit'] = 'hit'
        msg['miss'] = 'miss'
        msg['damage'] = 'damage'
        msg['long_damage'] = 'Target Damaged'
        msg['ready_fire'] = 'Ready to Fire'
        msg['loading'] = 'Loading Cannon Ball'
        msg['count_fire'] = 'Gun Fire Count：{0}'
        msg['count_hit'] = 'Hit Count:{0} , Damage:{1:.1f}'
        msg['distance'] = 'Target Distance:{0}M'
        msg['win'] = 'Win'
        msg['fs'] = 'Full'
        msg['quit'] = 'Quit'
        msg['fail'] = 'Mission Failed'
        msg['success'] = 'You dammage target beyound {0} meters!'
        msg['fire_dist'] = 'Enemy Dist:{0}M'
module_init()
